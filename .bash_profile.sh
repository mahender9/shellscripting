# .bash_profile

# Get the aliases and functions
if [ -f ~/.bashrc ]; then
        . ~/.bashrc
fi

# User specific environment and startup programs
#ANT_HOME=/home/udeploy/ant-1.9.7
#export ANT_HOME

export ANT_HOME=/home/udeploy/apache-ant-1.9.11
export PATH=$PATH:$ANT_HOME/bin

ANDROID_HOME=/home/udeploy/Android
export ANDROID_HOME

JAVA_HOME=/home/udeploy/java1.8/jdk1.8.0_111
export JAVA_HOME

export M2_HOME=/home/udeploy/maven/apache-maven-3.3.9
export M2=$M2_HOME/bin

export CATALINA_HOME=/home/udeploy/tomcat-7.0.63

PATH=$PATH:$HOME/.local/bin:$HOME/bin:/home/udeploy/sonar-scanner-2.8/bin:/home/udeploy/ant-1.9.7/bin:$JAVA_HOME/bin:/home/udeploy/maven/apache-maven-3.3.9/bin:$CATALINA_HOME/bin:$ANDROID_HOME/tools:$ANDROID_HOME/platform-tools:/home/udeploy/GO/go/bin:/home/udeploy/node-v8.11.4-linux-x64/bin

export PATH

#GOLANG PATH SETUP

export GOROOT=/home/udeploy/GO/go
export GOPATH=/home/udeploy/GO/workspace

export PATH="/home/udeploy/HPE_Security/Fortify_SCA_and_Apps_17.10/bin:$PATH"


export https_proxy=prodproxy.jio.com:8080
export http_proxy=prodproxy.jio.com:8080
