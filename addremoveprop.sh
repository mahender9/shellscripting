#--Farheen

if [ "$add_prop" == "yes" ];then
CONTENT="<property>\n<name>Property file</name>\n<option>java.property.TIBCO_SECURITY_VENDOR</option>\n<default>j2se</default>\n<description>required for SSL configuration</description>\n</property>"
cd /app/tibcobw513/bw/5.13/lib/com/tibco/deployment
cp bwengine.xml bwengine.xml$(date '+%d%m%y')
sed -i -e '/<\/properties>/i\'"$CONTENT" bwengine.xml
else
cd /app/tibcobw513/bw/5.13/lib/com/tibco/deployment
cp bwengineorig.xml bwengine.xml
fi
